FROM python:3.8-slim-buster
RUN mkdir /app
WORKDIR /app
COPY . .
RUN pip install flask
CMD [ "python3", "-m" , "flask", "run", "--host=0.0.0.0"]


